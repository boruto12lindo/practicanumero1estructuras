/*
 * Autor: Melina Karen Ticra Aguilar
 */
package practicanumero1;
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
public class Ejercicio2 
{
   public static void main(String[] args) 
    {
        String nombre = JOptionPane.showInputDialog("Ingrese el nombre del alumno");
        String uno = JOptionPane.showInputDialog("introduzca la primer evaluacion del alumno");
        double a=Double.parseDouble(uno);
        String dos = JOptionPane.showInputDialog("introduzca la segunda evaluacion del alumno");
        double b=Double.parseDouble(dos);
        String tres = JOptionPane.showInputDialog("introduzca la tercera evaluacion del alumno");
        double c=Double.parseDouble(tres);
        JOptionPane.showMessageDialog(null,"el promedio del alumno "+nombre+ " es  " + datos (a,b,c) );
    }
    public static double datos( double a, double b, double c)
    {
       double suma;
       double prom;
       suma= a+b+c;
       prom=suma/3;
       return prom;
    }
}

/*
 * Autor: Melina Karen Ticra Aguilar
 */
package practicanumero1;
import javax.swing.JOptionPane;
public class Ejercicio7 
{
    public static void main(String[] args) 
    {
        String v = JOptionPane.showInputDialog("Ingrese un numero");
        int con=Integer.parseInt(v);
        JOptionPane.showMessageDialog(null,"el numero de cifras es: "+contar(con));
    }
    public static int contar(int con)
    {
        int n=1;
        for(int i=0;i<=con;i++)
        {
           con=con/10;  
           n++;
        }
        return n;
    }
}

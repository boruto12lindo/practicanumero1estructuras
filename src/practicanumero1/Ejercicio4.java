/*
 * Autor: Melina Karen Ticra Aguilar
 */
package practicanumero1;
import javax.swing.JOptionPane;
public class Ejercicio4 
{
    public static void main(String[] args) 
    {
        String cr = JOptionPane.showInputDialog("introduzca un caracter");
        
        char l = (char)(cr).charAt(0);
        if(letra(l)==l)
        {
            JOptionPane.showMessageDialog(null," la letra es mayuscula");
        }
        else
        {
            JOptionPane.showMessageDialog(null,"la letra es minuscula ");
        }
        
    }
    public static char letra(char l)
    {
        return (Character.toUpperCase(l));
    }
}

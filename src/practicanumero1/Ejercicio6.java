/*
 *Autor: Melina Karen Ticra Aguilar
 */
package practicanumero1;
import javax.swing.JOptionPane;
public class Ejercicio6 
{
    public static void main(String[] args) 
    {
        String c=JOptionPane.showInputDialog("Inserta un numero");
        int num=Integer.parseInt(c);
        String bin=decimal(num);
        JOptionPane.showMessageDialog(null,bin);
    }
   public static String decimal (int dec)
   {
        String binario="";
        String digito;
        for(int i=dec;i>0;i/=2)
        {
            if(i%2==1)
            {
                digito="1";
            }
            else
            {
                digito="0";
            }
            binario=digito+binario;
        }
        return binario;


    }
}

/*
 * Autor: Melina Karen Ticra Aguilar
 */
package practicanumero1;

import javax.swing.JOptionPane;
public class Ejercicio5 
{
    public static void main(String[] args)
    {
     
        
        String v = JOptionPane.showInputDialog("Ingrese un valor para el divisor");
        double a=Double.parseDouble(v);
        String u = JOptionPane.showInputDialog("Ingrese un valor para el dividendo");
        double b=Double.parseDouble(u);
        while(a==0)
        {
            JOptionPane.showMessageDialog(null,"el resultado invalido, divisor debe ser mayor a 0");
            String t = JOptionPane.showInputDialog("Ingrese nuevamente un valor para el divisor");
            a=Double.parseDouble(t);
            String w = JOptionPane.showInputDialog("Ingrese un valor para el dividendo");
            b=Double.parseDouble(w);
            if(a!=0)
                break;
        }
        JOptionPane.showMessageDialog(null,"el resultado es: "+dividir(a,b) );
    }
    public static double dividir(double a, double b)
    {    
        return a/b;
    }
           
}

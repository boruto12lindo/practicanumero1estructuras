/*
 * Autor: Melina Karen Ticra Aguilar
 */
package practicanumero1;
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
public class PracticaNumero1 
{
            public static double suma(double a, double b)
    {
        return a + b;
    }
    public static double resta(double a, double b)
    {
        return a - b;
    }  
    public static double division(double a, double b){
        return a/b;
    }
    
    public static double multiplicacion(double a, double b){
        return a*b;
    }
    public static void main(String[] args) 
    {
        
        String t= JOptionPane.showInputDialog("Inserte un numero");
        double a=Double.parseDouble(t);
        String s= JOptionPane.showInputDialog("Inserte un numero mas");
        double b=Double.parseDouble(s);
        JOptionPane.showMessageDialog(null,"La resta es: "+ resta(a,b));
        JOptionPane.showMessageDialog(null,"su division es: "+ division(a,b));
        JOptionPane.showMessageDialog(null,"La multiplicacion es: "+ multiplicacion(a,b));
        JOptionPane.showMessageDialog(null,"La suma es: "+ suma(a,b));
    }
}

/*
 *Melina Karen Ticra Aguilar
 */
package practicanumero1;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class Ejercicio8 
{
    public static void main(String[] args) {
        String t=JOptionPane.showInputDialog("Inserta una cantidad bolivianos");
        double cant=Double.parseDouble(t);
        String mon=JOptionPane.showInputDialog("Escribe el tipo de moneda a la que quieres convertir");
        JOptionPane.showMessageDialog(null, convert(cant, mon));
    }
    public static String convert (double cant, String mon){
        double res=0;
        switch (mon)
        {
        case "libras":
            res=cant*0.12;
            break;
        case "dolares":
            res=cant*0.14;
            break;
        case "euros":
            res=cant*0.13;
            break;
        default:
           JOptionPane.showMessageDialog(null, "error");
        }
        DecimalFormat df = new DecimalFormat("#.00");
        return cant+ " bolivianos en " +mon+ " son " +df.format(res);
        
 
    }
}

/*
 * Melina Karen Ticra Aguilar
 */
package practicanumero1;
import javax.swing.JOptionPane;
public class Ejercicio10 
{
    public static void main(String[] args)
    {
        String n=JOptionPane.showInputDialog("Inserta un numero de tres cifres");
        int numero = Integer.parseInt(n);
        if(contarCifras(numero))
        {
            JOptionPane.showMessageDialog(null, cifras(numero));
        }
        else
        {
            JOptionPane.showMessageDialog(null, "error");
        }
    }
    public static String cifras (int numero)
    {
        return (numero/100)+"  "+((numero/10)%10)+"  " + (numero%10);
    }
    public static boolean contarCifras (int numero)
    {
         int cont=0;
         for (int i=numero;i>0;i/=10){
             cont++;
         }
         if(cont == 3){
             return true;
         }else{
             return false;
         }
    }

}
